// Se importa el módulo 'axios'
const axios = require('axios')
// Se define la URL de la API 
const url = 'https://jsonplaceholder.typicode.com/users'

// Se realiza una solicitud GET a la URL utilizando axios
//Recorre la lista de usuarios y imprime el nombre de usuario de cada uno
axios.get(url).then(response => {
    response.data.forEach(element => {
        console.log(element.username)
    });
})

//Realiza una solicitud POST para agregar el usuario "Foo Bar" y el correo electrónico" 
//Muestra la respuesta del servidor en la consola
axios.post(url, {
    username: "Foo Bar",
    email: "foo@bar.com"
}).then(response => console.log(response.data))